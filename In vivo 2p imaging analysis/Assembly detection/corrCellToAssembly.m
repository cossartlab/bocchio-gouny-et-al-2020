function [posCorrCells,negCorrCells,meanCorrCells,corr_cellAssembly,meanCorr_cellAssembly] = corrCellToAssembly(spikeCount,spikeCount_shuffled,assemblyActivities,maxLag,si,cellToPlot);

signBinsNum = 2; %number of consecutive significant bins to be found to define a cell correlated
binWindow = 5; % bin window to detect significant response. default:5

binLag = (binWindow-1)./2; 
 
nCells = size(spikeCount,1);
nAssemblies = size(assemblyActivities,1);

assembliesToPlot = 6;

if nAssemblies < assembliesToPlot
    assembliesToPlot = nAssemblies;
end

corr_cellAssembly = zeros(nAssemblies,maxLag*2+1,nCells);   %preallocation
corr_cellAssembly_shuff = zeros(nAssemblies,maxLag*2+1,nCells);

% find correlation between each cell and each assembly (store in 3d array: nAssemblies x nLags x nCells)
for cellCounter = 1:nCells
    for assemblyCounter = 1:nAssemblies
        [corr_cellAssembly(assemblyCounter,:,cellCounter),lags_cellAssembly] = xcorr(assemblyActivities(assemblyCounter,:),spikeCount(cellCounter,:),maxLag,'coeff');
    end
end

lags = lags_cellAssembly;
lags = lags .* si *10^-3; % lags vector (in seconds)

zeroLagPnt = find(lags_cellAssembly == 0);  %point in xcorr corresponding to zero lag

% plot correlation of example cell to each assembly
figure;
for assemblyCounter = 1:assembliesToPlot
    subplot(3,floor(assembliesToPlot/3)+1,assemblyCounter)
    bar(lags,corr_cellAssembly(assemblyCounter,:,cellToPlot))
    ylim([min(corr_cellAssembly(assemblyCounter,:,cellToPlot)) max(corr_cellAssembly(assemblyCounter,:,cellToPlot))])
    ylabel('Correlation to assembly activity')
    xlabel('Lag (s)')
end
subplot(3,3,2)
title('Original spike raster')

% find correlation between each cell and each assembly (store in 3d array: nAssemblies x nLags x nCells)
% and thresholds to define correlated cells (stored in 2d array: nAssemblies x nCells)
corr_highThresh = zeros(nAssemblies,nCells);    %preallocation
corr_lowThresh = zeros(nAssemblies,nCells);

for cellCounter = 1:nCells
    for assemblyCounter = 1:nAssemblies
        [corr_cellAssembly_shuff(assemblyCounter,:,cellCounter),~] = xcorr(assemblyActivities(assemblyCounter,:),spikeCount_shuffled(cellCounter,:),maxLag,'coeff');
        corr_highThresh(assemblyCounter,cellCounter) = prctile (corr_cellAssembly_shuff(assemblyCounter,:,cellCounter),95);
        corr_lowThresh(assemblyCounter,cellCounter) = prctile (corr_cellAssembly_shuff(assemblyCounter,:,cellCounter),5);
    end
end

% plot correlation of example cell to each assembly (reshuffled spikes)
figure;
for assemblyCounter = 1:assembliesToPlot
    subplot(3,floor(assembliesToPlot/3)+1,assemblyCounter)
    bar(lags,corr_cellAssembly_shuff(assemblyCounter,:,cellToPlot))
    ylim([min(corr_cellAssembly_shuff(assemblyCounter,:,cellToPlot)) max(corr_cellAssembly_shuff(assemblyCounter,:,cellToPlot))])
    ylabel('Correlation to assembly activity')
    xlabel('Lag (s)')
end
subplot(3,3,2)
title('Reshuffled spike raster')


% mean correlation of each cell to assemblies
meanCorr_cellAssembly = squeeze(mean(corr_cellAssembly,1));
meanCorr_cellAssembly_shuff = squeeze(mean(corr_cellAssembly_shuff,1));

% plot mean correlation of example cell to assemblies
figure;
subplot(2,1,1)
bar(lags,meanCorr_cellAssembly(:,cellToPlot))
ylim([min(meanCorr_cellAssembly(:,cellToPlot)) max(meanCorr_cellAssembly(:,cellToPlot))])
ylabel('Mean correlation to assembly activity')
xlabel('Lag (s)')
title('Original spike raster')
subplot(2,1,2)
bar(lags,meanCorr_cellAssembly_shuff(:,cellToPlot))
ylim([min(meanCorr_cellAssembly_shuff(:,cellToPlot)) max(meanCorr_cellAssembly_shuff(:,cellToPlot))])
ylabel('Mean correlation to assembly activity')
xlabel('Lag (s)')
title('Reshuffled spike raster')


% find cells with a significant mean correlation to assemblies
meanCorrCells = zeros(nCells,1); 

for cellCounter = 1:nCells
    meanCorr_thresh = prctile(meanCorr_cellAssembly_shuff(:,cellCounter),95);
    [meanCorr,~] = consecAboveThresh(meanCorr_cellAssembly(zeroLagPnt-binLag:zeroLagPnt+binLag,cellCounter),meanCorr_thresh,2);
    if length(meanCorr) > 2;
       meanCorrCells(cellCounter,1) = 1;
    end                
end

meanCorrCells = find(meanCorrCells);

% find cells significantly correlated to single assemblies
posCorrCells = zeros(nAssemblies,nCells);   %preallocation
negCorrCells = zeros(nAssemblies,nCells);

for cellCounter = 1:nCells
    for assemblyCounter = 1:nAssemblies
        
        [posCorr,~] = consecAboveThresh(corr_cellAssembly(assemblyCounter,zeroLagPnt-binLag:zeroLagPnt+binLag,cellCounter),corr_highThresh(assemblyCounter,cellCounter),signBinsNum-1);
        [negCorr,~] = consecAboveThresh(-corr_cellAssembly(assemblyCounter,zeroLagPnt-binLag:zeroLagPnt+binLag,cellCounter),-corr_lowThresh(assemblyCounter,cellCounter),signBinsNum-1);
        if length(posCorr) > 2;
            posCorrCells(assemblyCounter,cellCounter) = 1;
        end
        
        if length(negCorr) > 2;
            negCorrCells(assemblyCounter,cellCounter) = 1;
        end
                    
    end
end

end


