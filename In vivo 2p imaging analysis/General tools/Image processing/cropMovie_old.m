function [croppedMovie] = cropMovie(inputMovie,croppedPixels)

nPixels = size(inputMovie,1);
nFrames = size(inputMovie,3);

if length(croppedPixels) == 1

    nCroppedMoviePixels = nPixels - (croppedPixels*2);
    croppedMovie = zeros(nCroppedMoviePixels,nCroppedMoviePixels,nFrames);

    for frameCount = 1:size (inputMovie,3);
        croppedMovie(:,:,frameCount) = inputMovie(1+croppedPixels:end-croppedPixels,1+croppedPixels:end-croppedPixels,frameCount);
    end

else
    
    croppedMovie = zeros(
    
end



end

M3_cropped2=M3(croppedPixels+1:end-croppedPixels,croppedPixels+1:end-croppedPixels,:);
