function [croppedMovie] = cropMovie(inputMovie,croppedPixels)

if length(croppedPixels) == 1 
    croppedMovie = inputMovie(croppedPixels+1:end-croppedPixels,croppedPixels+1:end-croppedPixels,:);
else
    croppedMovie = inputMovie(croppedPixels(1)+1:end-croppedPixels(2),croppedPixels(3)+1:end-croppedPixels(4),:);
end

end

