%--------------------------------------------------------------------------
% Pipeline to analyse assembly activities in large groups of imaged cells
%--------------------------------------------------------------------------

% % >>> REQUIRED THIRD-PARTY CODES >>>
%   -   toolbox (PCA/ICA method, Vitor Lopes-Dos Santos, vtlsantos@gmail.com)
% 
%--------------------------------------------------------------------------
% Marco Bocchio 07.2019
%--------------------------------------------------------------------------
% Updates and revisions
% % 06.08.19  fixed bugs with definition of single and multiple assembly cell 


%% Exclude cells from assembly detection

cellsToExclude = [nCells.class2];

spikenums.rest.assemblies = spikenums.rest.binned;
spikenums.rest.assemblies(cellsToExclude,:) = [];
spikenums.rest.assemblies_shuffled=spikenums.rest.binned_shuffled;
spikenums.rest.assemblies_shuffled(cellsToExclude,:)=[];

nCells.assemblies = size(spikenums.rest.assemblies,1);




%% ICA_assembly

%spikenums.rest.binned = spikenums.tot.binned(:,restEpochsIndex);
%[spikenums.rest.shuffled] = circshiftRaster (spikenums.rest.binned,1000,'noPlotting');

 
% Plot correlation matrix
ICA_results.correlationmat = corr(spikenums.rest.assemblies');
figure
imagesc(ICA_results.correlationmat)

    %Options
clear opts;
opts.threshold.permutations_percentile = 99;
opts.threshold.number_of_permutations = 1000;
opts.Patterns.number_of_iterations = 500;
opts.threshold.method = 'circularshift';
opts.Patterns.method = 'ICA';

    % Assembly patterns
ICA_results.Patterns = assembly_patterns(spikenums.rest.assemblies,opts);
ICA_results.nAssemblies = size(ICA_results.Patterns,2);

   
    %Assembly activities
ICA_results.Activities = assembly_activity(ICA_results.Patterns,spikenums.rest.assemblies);

if exist('assemblyMembers')==1
    clear assemblyMembers
end

ICA_results.cellsInAssemblies = [];

figure;
% Cell participation and plot of assembly patterns
for assemblyCounter = 1:ICA_results.nAssemblies
   % Find cells that are part of an assembly (>1.5 standard deviation of
        % weights)
    highestWeightSign = abs(max(ICA_results.Patterns(:,assemblyCounter)))-abs(min(ICA_results.Patterns(:,assemblyCounter)));
    if highestWeightSign > 0
        assemblyMembers_temp = find(zscore(ICA_results.Patterns(:,assemblyCounter))>1.5);
    else
        assemblyMembers_temp = find(zscore(ICA_results.Patterns(:,assemblyCounter))<-1.5);
    end
    ICA_results.assemblyMembers{assemblyCounter} = assemblyMembers_temp;
    ICA_results.cellsInAssemblies = vertcat(ICA_results.cellsInAssemblies, assemblyMembers_temp);
    
        % Plot assembly weights
    subplot(ICA_results.nAssemblies,1,assemblyCounter)
    stem(ICA_results.Patterns(:,assemblyCounter))
end

ICA_results.cellsInAssemblies = unique(ICA_results.cellsInAssemblies);
ICA_results.propCellsInAssemblies = length(ICA_results.cellsInAssemblies)./nCells.assemblies;

% number of cells per assembly
ICA_results.assemblySize = zeros(ICA_results.nAssemblies,1); 
for assemblyCounter = 1:ICA_results.nAssemblies
        ICA_results.assemblySize(assemblyCounter) = length(ICA_results.assemblyMembers{1, assemblyCounter}  );
end
ICA_results.meanAssemblySize = mean(ICA_results.assemblySize);

         
% assembly overlap (cells shared in multiple assemblies)
    %concatenate cells forming assemblies in one vector
for assemblyCounter = 1:ICA_results.nAssemblies
    if assemblyCounter == 1
    concCellAssemblies = ICA_results.assemblyMembers{1,1};
    end
    if assemblyCounter > 1
    concCellAssemblies = vertcat(concCellAssemblies,ICA_results.assemblyMembers{1,assemblyCounter});
    end
end

    %pre-allocation
ICA_results.singleAssemblyCells = zeros(length(ICA_results.cellsInAssemblies),1);
ICA_results.multiAssemblyCells = zeros(length(ICA_results.cellsInAssemblies),1);

    % find single assembly and multiple assemblies cells
for cellAssemblyCounter = 1:length(ICA_results.cellsInAssemblies);
    cellInAssembly_idx = find(concCellAssemblies == ICA_results.cellsInAssemblies(cellAssemblyCounter));
    if length(cellInAssembly_idx) == 1
        ICA_results.singleAssemblyCells(cellAssemblyCounter,1) = ICA_results.cellsInAssemblies(cellAssemblyCounter);
    else
        ICA_results.multiAssemblyCells(cellAssemblyCounter,1) = ICA_results.cellsInAssemblies(cellAssemblyCounter);
    end
end
      % remove zero elements
ICA_results.singleAssemblyCells(ICA_results.singleAssemblyCells==0) = [];
ICA_results.multiAssemblyCells(ICA_results.multiAssemblyCells==0) = [];

    % assembly overlap score (n multi assembly cells / n tot cells forming
    % assemblies)
ICA_results.assemblyOverlap = length(ICA_results.multiAssemblyCells)./length(ICA_results.cellsInAssemblies);
    
clear uniqueCellAssemblies_idx concCellAssemblies

% Plot graphs
figure
subplot(211)
imagesc(spikenums.rest.assemblies)
xlim([0 length(spikenums.rest.assemblies)])
subplot(212)
plot(ICA_results.Activities')
xlim([0 length(spikenums.rest.assemblies)])
legend

% Save results
fileName_ICA_assembly=strcat(fileName,'_ICA_results.mat');
save(fileName_ICA_assembly,'fileName', 'ICA_results','-v7.3');
clear fileName_ICA_assembly;

%% Cross-correlation of single cell activities to assembly activities (ICA)

cellToPlot = 1;
maxLag = 20;

[ICA_xcorr_results.posCorrCells,ICA_xcorr_results.negCorrCells,ICA_xcorr_results.meanCorrCells,ICA_xcorr_results.corr_cellAssembly,ICA_xcorr_results.meanCorr_cellAssembly] = corrCellToAssembly(spikenums.rest.binned,spikenums.rest.binned_shuffled,ICA_results.Activities,maxLag,si_SCE,cellToPlot);

ICA_xcorr_results.nCorrAssemblies = sum(ICA_xcorr_results.posCorrCells,1);

ICA_xcorr_results.highRespCells = intersect(respCells.mvmPlusSCEonCells,ICA_xcorr_results.meanCorrCells);

fileName_ICA_assembly_xcorr=strcat(fileName,'_ICA_assembly_xcorr.mat');
save(fileName_ICA_assembly_xcorr,'fileName', 'ICA_xcorr_results','-v7.3');
clear fileName_ICA_assembly_xcorr;

