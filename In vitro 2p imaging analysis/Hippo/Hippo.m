% save('c:\hippobackup.mat')
clear;

versionnum = 3.1;
%hippopath = fullfile('C:\Program Files\MATLAB\R2006b\','work\Hippo');
%hippopath='C:\Users\feldt\Documents\UCI research\INMED research\SarahMatlabCode\current_hippo_code\Hippo'
all_path=path;
my_path=regexp(all_path, '\;', 'split');
if length(my_path) <=1
    my_path=regexp(all_path, ':', 'split');
    yes_mac=1;
else
    yes_mac=0;
end
hippo_dir=my_path{2};
if yes_mac==-2
    hippopath=[hippo_dir, '\Hippo'];
end
if yes_mac==1
    hippopath=[hippo_dir, '/Hippo'];
end

opengl neverselect;
fig = figure('Name',['Hippo ' num2str(versionnum,'%6.1f')],'NumberTitle','off','MenuBar','none','doublebuffer','on',...
    'units','normalized','closerequestfcn','hippofigclose','position',[0 .08/3 1 2.86/3]);

%Image functions
uicontrol('Style','text','Units','normalized','String','Image','Position',[.87 .955 .11 0.03],'FontSize',12,'FontWeight','Bold','BackgroundColor',[.8 .8 .8]);
bopenimage = uicontrol('Style','pushbutton','Units','normalized','String','Open','Position',[.87 .91 .05 .03],'FontSize',9, ...
    'Callback','HippoOpenImage');
bzoom = uicontrol('Style','pushbutton','Units','normalized','String','Zoom','Position',[.93 .91 .05 .03],'FontSize',9, ...
    'Callback','zoom on','Enable','off');
uicontrol('Style','text','units','normalized','string','Brightness','position',[.87 .88 .11 .02],'FontSize',9,'BackgroundColor',[.8 .8 .8]);
bbright = uicontrol('Style','slider','Units','normalized','Position',[.87 .86 .11 .02],'Min',0,'Max',1,'Sliderstep',[.01 .05],'Value',1/3, ...
    'Enable','off','Callback','HippoContrast');
uicontrol('Style','text','units','normalized','string','Contrast','position',[.87 .83 .11 .02],'FontSize',9,'BackgroundColor',[.8 .8 .8]);
bcontrast = uicontrol('Style','slider','Units','normalized','Position',[.87 .81 .11 .02],'Min',0,'Max',1,'Sliderstep',[.01 .05],'Value',1/3, ...
    'Enable','off','Callback','HippoContrast');

res_title = uicontrol('Style','text','Units','normalized','String','Resolution','Position',[.87 .755 .11 0.03],'FontSize',12,'FontWeight','Bold','BackgroundColor',[.8 .8 .8]);
txlabsr = uicontrol('Style','text','Units','normalized','String','Spatial (�m/pixel)','Position',[.87 .715 .11 0.02],'FontSize',9,...
    'BackgroundColor',[.8 .8 .8],'HorizontalAlignment','left');
inptsr = uicontrol('Style','edit','Units','normalized','String','1.2','Position',[.87 .715-0.0275 .11 0.025],'FontSize',9,...
    'BackgroundColor',[1 1 1],'HorizontalAlignment','left','enable','off');
txlabtr = uicontrol('Style','text','Units','normalized','String','Temporal (sec/frame)','Position',[.87 .715-0.0275-0.025 .11 0.02],'FontSize',9,...
    'BackgroundColor',[.8 .8 .8],'HorizontalAlignment','left');
inpttr = uicontrol('Style','edit','Units','normalized','String','.1','Position',[.87 .715-2*0.0275-0.025 .11 0.025],'FontSize',9,...
    'BackgroundColor',[1 1 1],'HorizontalAlignment','left','enable','off');

bnext = uicontrol('Style','pushbutton','Units','normalized','String','Next >>','Position',[.93 .02 .05 .03],'FontSize',9, ...
    'Enable','off','Callback','HippoDefineBorders');

logoim = subplot('position',[0.25 0.3 0.4 0.4]);
a = imread('hippo.bmp');
imagesc(a);
axis equal
axis off
logoname = uicontrol('Style','text','Units','normalized','String',['HIPPO ' num2str(versionnum,'%6.1f')],'Position',[.25 .7 .4 .05],'HorizontalAlignment','left', ...
    'FontSize',18,'FontWeight','Bold','BackgroundColor',[.8 .8 .8],'foregroundcolor',[1 0 0]);
myname = uicontrol('Style','text','Units','normalized','String','by Dmitriy Aronov (da2006@columbia.edu)','Position',[.25 .2 .4 .05],'HorizontalAlignment','right', ...
    'FontSize',14,'FontWeight','Bold','BackgroundColor',[.8 .8 .8],'foregroundcolor',[1 0 0]);