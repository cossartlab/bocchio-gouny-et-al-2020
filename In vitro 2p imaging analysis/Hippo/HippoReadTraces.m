stV = get(dpreaders,'value');
if isfield(region,'traces')==0 % paolo added
    if strcmp(st{stV},'None')
        [tr, trhalo, param] = feval('HippoTR_None',fnm,region);
            region.frStart=frStart;
            region.frEnd=frEnd;
        region.tracereadername = 'HippoTR_None';
    else
        if frameS=='y'
            [tr, trhalo, param] = feval('HippoTR_ReadTraceFrSel',fnm,region,frStart,frEnd);
            tr=tr(:,frStart:frEnd);
            region.frStart=frStart;
            region.frEnd=frEnd;
            region.tracereadername = 'HippoTR_ReadTraceFrSel';
        else
            [tr, trhalo, param] = feval('HippoTR_ReadTracesAll',fnm,region);
            region.frStart=1;
            region.frEnd=size(tr,2);
            region.tracereadername = 'HippoTR_ReadTracesAll';
        end
    end

    region.traces = tr;
    region.halotraces = trhalo;
    region.tracereaderparam = param;
end % paolo added

delete(get(fig,'children'));

uicontrol('Style','text','Units','normalized','String','Signals','Position',[.87 .955 .11 0.03],'FontSize',12,'FontWeight','Bold','BackgroundColor',[.8 .8 .8]);


halo_check = uicontrol('Style','checkbox','Units','normalized','String','Show halo traces','Position',[.87 .915 .11 0.025],'FontSize',9,...
    'BackgroundColor',[.8 .8 .8],'Callback','HippoPlotTrace');
if region.halomode == 0
    set(halo_check,'enable','off');
end

df_check = uicontrol('Style','checkbox','Units','normalized','String','Calculate DF/F','Position',[.87 .885 .11 0.025],'FontSize',9,...
    'BackgroundColor',[.8 .8 .8],'Callback','HippoPlotTrace');

numslider = uicontrol('Style','slider','Units','normalized','Position',[0.1 0.05 0.74 0.03],'Callback','HippoPlotTrace',...
    'Min',1,'Max',length(region.contours),'Sliderstep',[1/length(region.contours) 10/length(region.contours)],'Value',1);

subplot('position',[0.1 0.6 0.74 0.35]);
hold on;
cl = hsv(length(region.name));
cnt = zeros(1,length(region.contours));
for c = 1:length(region.contours)
    cnt(c) = patch(region.contours{c}([1:end 1],1),region.contours{c}([1:end 1],2),[0 0 0]);
    set(cnt(c),'edgecolor',cl(region.location(c),:));
    set(cnt(c),'ButtonDownFcn',['set(numslider,''value'',' num2str(c) '); HippoPlotTrace;']);
end
axis equal
imagesize = size(region.image);
xlim([0 imagesize(2)])
ylim([0 imagesize(1)])
set(gca,'ydir','reverse');
box on
set(gca,'color',[0 0 0]);
set(gca,'xtick',[],'ytick',[]);

currdir = pwd;
cd(fullfile(hippopath,'signaldetectors'));
mt = dir('*.m');
cd(currdir);

st = cell(1,length(mt));
for c = 1:length(mt)
    st{c} = mt(c).name(1:end-2);
    if strcmp(upper(st{c}(1:min([8 length(st{c})]))),'HIPPOSD_')
        st{c} = st{c}(9:end);
    end
end

% dummy(1) = uicontrol('Style','text','Units','normalized','String','Signal detector','Position',[.87 0.8425 .11 0.02],'FontSize',9,...
%     'HorizontalAlignment','left','BackgroundColor',[.8 .8 .8]);
% dpdetectors = uicontrol('Style','popupmenu','Units','normalized','String',st,'Position',[.87 .8175 .11 0.025],'FontSize',9,...
%     'BackgroundColor',[1 1 1]);
% btdetect = uicontrol('Style','pushbutton','Units','normalized','String','Detect!','Position',[.93 .7725 .05 0.03],'FontSize',9,...
%     'Callback','HippoDetectSignals');

bnext = uicontrol('Style','pushbutton','Units','normalized','String','Finish','Position',[.93 .02 .05 .03],'FontSize',9, ...
    'Enable','on','Callback','HippoFinish');

region.onsets = cell(1,length(region.contours));
region.offsets = cell(1,length(region.contours));
th = [];
HippoPlotTrace;