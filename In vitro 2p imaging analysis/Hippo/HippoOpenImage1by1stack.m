[filename, pathname] = uigetfile({'*.tif'}, 'Choose image to open');
% if ~isstr(filename)
%     return
% end
fnm = [pathname filename];

    hi=imread(fnm,1);
    [info(1).Height info(1).Width]=size(hi);
    


answer = inputdlg('Do you know how many frames does it have? (if yes write the number, if not don''t write anything','number of frames');
answer3 = inputdlg('Do you want to select a seqeunce of frames? (if yes write the 1st frame, if not don''t write anything','frame Start');
if isempty(answer3{1})==0
    frStart=str2num(answer3{1});
    frEndd = inputdlg('write the end frame','frEnd');
    frEnd=str2num(frEndd{1});
else
    frStart=1;
end
if isempty(answer) & isempty(answer3)
    info = imfinfo(fnm);
    numframes = length(info);
    frEnd=numframes;
elseif isempty(answer)==0 & isempty(answer3)
    numframes = str2num(answer{1});
    frEnd=numframes;
end
numframes=frEnd-frStart+1;


pr = zeros(1,numframes,3);
region = [];

if numframes == 1
    a = double(imread(fnm));
    button = questdlg({'The file contains one frame.','How was it obtained?'},'Frame information','Average','Maximum','First frame','Average');
    if strcmp(button,'Average')
        region.frametype = 'average';
    elseif strcmp(button,'Maximum')
        region.frametype = 'maximum';
    elseif strcmp(button,'First frame')
        region.frametype = 'first';
    end
else
    button = questdlg({[num2str(frEnd-frStart+1) 'frames '],'Choose the operation to perform.'},'Frame information',...
        'Average','Maximum','First frame','Average');
    if strcmp(button,'Average')
        button2 = questdlg({['how many frames to average?'],'Choose the operation to perform.'},'Frame information',...
            'All','1/4','1/10','All');
        if strcmp(button2,'All')
            region.frametype = 'average';
            a = zeros(info(1).Height,info(1).Width);
            for c = frStart:frEnd
                if mod(c,20)==1
                    prg = subplot('position',[.87 .575 .11 0.025]);
                    imagesc(pr);
                    axis off
                    drawnow;
                    delete(prg);
                end
                if c<10
                    txt2add=['0000' num2str(c) '.tif'];
                end
                if c>=10 & c<100
                    txt2add=['000' num2str(c) '.tif'];
                end
                if c>=100 & c<1000
                    txt2add=['00' num2str(c) '.tif'];
                end
                if c>=1000 & c<10000
                    txt2add=['0' num2str(c) '.tif'];
                end
                if c>=10000 
                    txt2add=[num2str(c) '.tif'];
                end
                fnm2r=[fnm(1:end-9) txt2add];
                a = a + double(imread(fnm2r,1));
                pr(1,c,1) = 1;
            end
            a = a / numframes;
        elseif strcmp(button2,'1/4')
            pr = zeros(1,round(numframes/4),3);
            region.frametype = 'Average 1/4';
            hi=randperm(numframes);
            frRand=hi(1:round(numframes/4));
            a = zeros(info(1).Height,info(1).Width);
            hi=0;
            for c = frRand+frStart-1
                hi=hi+1;
                if mod(c,20)==1
                    prg = subplot('position',[.87 .575 .11 0.025]);
                    imagesc(pr);
                    axis off
                    drawnow;
                    delete(prg);
                end
                if c<10
                    txt2add=['0000' num2str(c) '.tif'];
                end
                if c>=10 & c<100
                    txt2add=['000' num2str(c) '.tif'];
                end
                if c>=100 & c<1000
                    txt2add=['00' num2str(c) '.tif'];
                end
                if c>=1000 & c<10000
                    txt2add=['0' num2str(c) '.tif'];
                end
                if c>=10000 
                    txt2add=[num2str(c) '.tif'];
                end
                fnm2r=[fnm(1:end-9) txt2add];
                a = a + double(imread(fnm2r,1));                
                pr(1,hi,1) = 1;
            end
            a = a / round(numframes/4) ;
        elseif strcmp(button2,'1/10')
            pr = zeros(1,round(numframes/10),3);
            region.frametype = 'Average 1/10';
            hi=randperm(numframes);
            frRand=hi(1:round(numframes/4));
            a = zeros(info(1).Height,info(1).Width);
            hi=0;
            for c = frRand+frStart-1
                hi=hi+1;
                if mod(c,20)==1
                    prg = subplot('position',[.87 .575 .11 0.025]);
                    imagesc(pr);
                    axis off
                    drawnow;
                    delete(prg);
                end
                if c<10
                    txt2add=['0000' num2str(c) '.tif'];
                end
                if c>=10 & c<100
                    txt2add=['000' num2str(c) '.tif'];
                end
                if c>=100 & c<1000
                    txt2add=['00' num2str(c) '.tif'];
                end
                if c>=1000 & c<10000
                    txt2add=['0' num2str(c) '.tif'];
                end
                if c>=10000 
                    txt2add=[num2str(c)  '.tif'];
                end
                fnm2r=[fnm(1:end-9) txt2add];
                a = a + double(imread(fnm2r,1));
                pr(1,hi,1) = 1;
            end
            a = a / round(numframes/10) ;
        end
    elseif strcmp(button,'Maximum')
        region.frametype = 'maximum';
        a = uint16(zeros(info(1).Height,info(1).Width,2));
        for c = frStart:frEnd
            if mod(c,20)==1
                prg = subplot('position',[.87 .575 .11 0.025]);
                imagesc(pr);
                axis off
                drawnow;
                delete(prg);
            end
                if c<10
                    txt2add=['0000' num2str(c) '.tif'];
                end
                if c>=10 & c<100
                    txt2add=['000' num2str(c) '.tif'];
                end
                if c>=100 & c<1000
                    txt2add=['00' num2str(c)  '.tif'];
                end
                if c>=1000 & c<10000
                    txt2add=['0' num2str(c) '.tif'];
                end
                if c>=10000 
                    txt2add=[num2str(c)  '.tif'];
                end
                fnm2r=[fnm(1:end-9) txt2add];
                
            a(:,:,2) = imread(fnm2r,1);
            a(:,:,1) = max(a,[],3);
            pr(1,c,1) = 1;
        end
        a = double(a(:,:,1));
    elseif strcmp(button,'First frame')
        region.frametype = 'first';
        
                fnm2r=[fnm(1:end-8) '00000.tif'];
        
        a = double(imread(fnm2r,1));
    end
end

delete(logoim)
delete(logoname)
delete(myname)

imgax = subplot('position',[0.02 0.02 0.82 0.96]);
imagesc(a);
hold on

set(gca,'xtick',[],'ytick',[]);
axis equal
axis tight
box on

colormap gray

set(bzoom,'enable','on');
set(bbright,'enable','on');
set(bcontrast,'enable','on');
set(bnext,'enable','on');
set(inptsr,'enable','on');
set(inpttr,'enable','on');


[maxy maxx] = size(a);

zoom on;
HippoContrast;