function y = oneGauss(beta,x);
y=beta(1)*exp(-(x-beta(2)).^2/beta(3));%+beta(4)*exp(-(x-beta(5)).^2/beta(6));
% y=(exp(-x))*beta(1).*exp(-(x-beta(2)).^2/beta(3));%  +(exp(-x)).*beta(4).*exp(-(x-beta(5)).^2/beta(6));