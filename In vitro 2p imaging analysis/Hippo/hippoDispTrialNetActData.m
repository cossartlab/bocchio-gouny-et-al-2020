st=get(dpreaders,'value');
button='no';
if strcmp(mt(st).name(1:end-2),'newSignalDetector_2ph')
    nr = inputdlg({'number of integration points?','thrshold for noise'},'parameters',1,{num2str(lastThIntPt),num2str(lastTh)});
end
if strcmp(mt(st).name(1:end-2),'old_newSignalDetector_2ph')
    nr = inputdlg({'number of integration points?','thrshold for noise'},'parameters',1,{num2str(lastThIntPt),num2str(lastTh)});
end


prg = zeros(1,size(tr,1)+1);
tfigg = figure('Name','spike detection','NumberTitle','off','doublebuffer','on','units','normalized','position',[0.3    0.5    0.4    0.025]);
subplot('position',[0 0 1 1]);
set(gca,'xtick',[],'ytick',[]);
for c = 1:size(tr,1);
    tic
    %     c
    size(tr,1)
    prg(c) = 1;
    figure(tfigg);
    imagesc(prg);
    set(gca,'xtick',[],'ytick',[]);
    drawnow
    
    switch mt(st).name(1:end-2)
        case 'HippoEvent_FastBest'
            [s d] = feval(mt(st).name(1:end-2),region,c,button,[],[]);
        case 'HippoEvent_FastBestProva'
            [s d] = feval(mt(st).name(1:end-2),region,c,button,[],[]);
        case 'HippoEvent_10x'
            [s d] = feval(mt(st).name(1:end-2),region,c,7);
        case 'old_newSignalDetector_2ph'
            [s d] = feval(mt(st).name(1:end-2),region,c,'no',str2num(nr{1}) ,str2num(nr{2}));
        case 'newSignalDetector_2ph'
            [s d] = feval(mt(st).name(1:end-2),region,c,'no',str2num(nr{1}) ,str2num(nr{2}));
        otherwise
            [s d] = feval(mt(st).name(1:end-2),region,c,button);
    end
    
    
    %     [s d] = feval(mt(st).name(1:end-2),region,c,button);
    %     if strcmp(mt(st).name(1:end-2),'HippoEvent_FastBest') | strcmp(mt(st).name(1:end-2),'HippoEvent_FastBestProva')
    %         [s d] = feval(mt(st).name(1:end-2),region,c,button,[],[]);
    %     else
    %         [s d] = feval(mt(st).name(1:end-2),region,c,button);
    %     end
    %     [s d] = hippodettrial(tr(c,:));
    %     set(progtx,''String'',[''Detecting '' num2str(c) '' of '' num2str(size(nt,1))]);
    spk(c,:) = 0;
    dec(c,:) = 0;
    spk(c,s) = 1;
    dec(c,d) = 1;
    region.onsets{c}=s;
    region.offsets{c}=d;
    toc
end;
close (tfigg)
% set(progtx,''String'','''');
hevPlotTraceHpEvWithNetAct;
